//
//  p02_wintertonTests.swift
//  p02-wintertonTests
//
//  Created by Gabrielle Winterton on 2/5/16.
//  Copyright © 2016 Gabrielle Winterton. All rights reserved.
//

import XCTest
@testable import p02_winterton

class p02_wintertonTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
