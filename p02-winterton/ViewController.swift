//
//  ViewController.swift
//  p02-winterton
//
//  Created by Gabrielle Winterton on 2/5/16.
//  Copyright © 2016 Gabrielle Winterton. All rights reserved.
//

import UIKit
import QuartzCore

class ViewController: UIViewController {

    var matrix = Array(count: 4, repeatedValue: Array(count: 4, repeatedValue: 0))
    var dir = 0
    var score = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //matrix[0][1] = 4
        //matrix[1][1] = 2
        //matrix[1][3] = 2
        //matrix[2][0] = 4
        //matrix[2][1] = 2
        //matrix[2][3] = 2
        //matrix[3][0] = 2
        //matrix[3][2] = 2
        //matrix[3][3] = 4
        
        gridInit()
        
        for x in 0 ... 15 {
            grid[x].layer.cornerRadius = 15
            grid[x].clipsToBounds = true
        }
        
        //self.view .sendSubviewToBack(Back)
        
        //showMatrix()
        
        //print(matrix)
        //move()
        //print(matrix)
        //makeVerticle()
        //print(matrix)
        //move()
        //print(matrix)
        //makeVerticle()
        //print(matrix)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet var grid: [UILabel]!
    @IBOutlet weak var ScoreNum: UILabel!
    @IBOutlet weak var Back: UIView!
    
    @IBAction func reset(sender: AnyObject) {
        gridInit()
    }
    
    @IBAction func direction(sender: AnyObject) {
        dir = sender.tag
        if dir%2 == 0 {
            makeVerticle()
            if dir == 2 {
                reverse()
            }
            move()
            if dir == 2 {
                reverse()
            }
            makeVerticle()
            
        }
        else {
            if dir == 1 {
                reverse()
            }
            move()
            if dir == 1 {
                reverse()
            }
        }
        //place random 2 in place that has a zero
        placeTwo()
        //showMatrix()
        print(" ")
        
        updateDisplay()
    }
    
    
    
    func move(){
        //print(matrix)
        var notDone = true
        while notDone {
            var i = 0; //to keep track of row number to update matrix
            for var row in matrix {
                //print(row)
                var temp = Array(count: 4, repeatedValue: 0)
                var tindex = 0
            
                for x in 0 ... 3 {
                    if row[x] != 0 {
                        temp[tindex] = row[x]
                        tindex++
                    }
            
                }
            
                for y in 0 ... 2 {
                    if temp[y] == temp[y+1] && temp[y] != 0 {
                        if score == 0 {
                            score = 2
                        }
                        else{
                            let n = score
                            score = n + temp[y]
                        }
                        ScoreNum.text = "\(score)"
                        temp[y] = temp[y]*2
                        temp[y+1] = 0
                    }
                
                }
                //print(temp)
                for j in 0 ... 3 {
                    matrix[i][j] = temp[j]
                }
                i++
            }
            //now check to see if there is still more to do
            var needsWork = false
            for row in matrix {
                for x in 0 ... 2 {
                    if row[x] != 0 {
                        if row[x] == row[x+1]{
                            needsWork = true
                        }
                    }
                    if row[x] == 0 {
                        if row[x+1] != 0 {
                            needsWork = true
                        }
                    }
                    
                }
            }
            if needsWork == false {
                notDone = false
            }
        }
    }
    
    func makeVerticle(){
        var temp = Array(count: 4, repeatedValue: Array(count: 4, repeatedValue: 0))
        var i = 0
        for row in matrix {
            for x in 0 ... 3 {
                temp[x][i] = row[x]
            }
            i++
        }
        matrix = temp
    }
    
    func reverse(){
        var i = 0
        for row in matrix {
            var temp = Array(count: 4, repeatedValue: 0)
            temp = row.reverse()
            for x in 0 ... 3 {
                matrix[i][x] = temp[x]
            }
            i++
        }
    }
    
    func gridInit(){
        //make matrix equal to zero

        for x in 0 ... 3 {
            for y in 0 ... 3 {
                matrix[x][y] = 0
            }
        }
        //place 2 random 2's on the board
        var rand1 = 0
        var rand2 = 0
        while(rand1 == rand2){
            rand1 = Int(arc4random_uniform(15))
            rand2 = Int(arc4random_uniform(15))
        }
        grid[rand1].text = "2"
        grid[rand2].text = "2"
        

        for x in 0 ... 3{
            for y in 0 ... 3 {
                matrix[x][y] = 0
                let num = (x * 4) + y
                if num == rand1 || num == rand2 {
                    matrix[x][y] = 2
                }
            }
        }
        //update display
        updateDisplay()
        ScoreNum.text = "0"
        
    }
    
    func updateDisplay(){
        var x = 0
        var y = 0
        for row in matrix {
            y = 0
            for col in row {
                let num = (x*4) + y
                if col == 0 {
                    grid[num].text = " "
                }
                else {
                    grid[num].text = "\(col)"
                }
                //print("x: \(x) y: \(y) num: \(num)")
                y++
            }
            x++
        }
    }
    
    func placeTwo() {
        //check if full
        let notFull = checkFull()
        if notFull {
            var rand = Int(arc4random_uniform(15))
            var y = rand % 4
            var x = (rand - y)/4
            while(matrix[x][y] != 0){
                rand = Int(arc4random_uniform(15))
                y = rand % 4
                x = (rand - y)/4
            }
            matrix[x][y] = 2
        }
    }
    
    func checkFull() -> Bool{
        var notFull = false
        for row in matrix {
            for col in row {
                if col == 0 {
                    notFull = true
                }
            }
        }
        return notFull
    }
    
    func showMatrix(){
        print("Matrix")
        for row in matrix {
            print(row)
        }
    }
    
}





